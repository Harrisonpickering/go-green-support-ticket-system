# Go Green Support Ticket System

The system was developed as an assessment for Go Green towards a job interview. All code has been provided along with installation
instructions below.

# Install

First go to the src directory and run composer

```
cd src
php composer.phar install
```

Next install the provided SQL file to your database. How you do this is up to you.

The default admin account details created when you import the SQL file are as follows:

```
Username: gogreen_admin
Password: password
```