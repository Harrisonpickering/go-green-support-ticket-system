(function() {
    $("#btn-close-ticket").click(() => {
        if(confirm('Are you sure you want to close the ticket?')) {
            $("#close-form").submit();
        }
    });

    $("#btn-reopen-ticket").click(() => {
        if(confirm('Are you sure you want to reopen the ticket?')) {
            $("#reopen-form").submit();
        }
    });
})();