(function() {
    $("#btn-delete-user").click(() => {
        if(confirm('Are you sure you want to delete the user?')) {
            $("#delete-adminPassword").val($("#adminPassword").val());
            $("#delete-form").submit();
        }
    });
})();