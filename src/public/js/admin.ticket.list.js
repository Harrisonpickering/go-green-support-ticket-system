(function() {
    $("#l").change(function() {
        $("#limit").val($(this).val());
        $("#filter_form").submit();
    });

    $("#a").change(function() {
        $("#assigned").val($(this).val());
        $("#filter_form").submit();
    });

    $("#c").change(function() {
        $("#client").val($(this).val());
        $("#filter_form").submit();
    });

    $("#t").keydown(function(e) {
        if(e.keyCode == 13) {
            $("#search").val($(this).val());
            $("#filter_form").submit();
        }
    });

    $("#s").change(function() {
        $("#status").val($(this).val());
        $("#filter_form").submit();
    });

    $("#oc").change(function() {
        $("#open_close").val($(this).val());
        $("#filter_form").submit();
    });
})();