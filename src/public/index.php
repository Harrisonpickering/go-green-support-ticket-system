<?php
// Autoload packages via composer autoload script
require_once __DIR__.'/../vendor/autoload.php';

// Start sessions
session_start();

// Create application with configuration loaded from dotenv (.env)
$app = new \Slim\App(require __DIR__.'/../app/config.php');

// Register services, middleware, classes, and routes
$container = $app->getContainer();
require_once __DIR__.'/../app/services.php';
require_once __DIR__.'/../app/middleware.php';
require_once __DIR__.'/../app/routes.php';

// Start application
$app->run();