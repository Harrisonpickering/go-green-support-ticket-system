<?php
// Register eloquent as "db" in application container
$container['db'] = function($c) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($c['settings']['db']);

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

// Register csrf as "csrf" in application container
$container['csrf'] = function($c) {
    return new \Slim\Csrf\Guard;
};

// Register twig templating as "view" in application container
$container['view'] = function($c) {
    $view = new \Slim\Views\Twig(__DIR__.'/../views', [
        'cache' => __DIR__.'/../views/cache',
        'auto_reload' => true,
    ]);

    $basePath = rtrim(str_ireplace('index.php', '', $c->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $basePath));
    $view->addExtension(new \STS\Extensions\CsrfExtension($c->get('csrf')));
    $view->addExtension(new \STS\Extensions\FlashExtension($c->get('flash')));
    $view->addExtension(new \STS\Extensions\AuthExtension());

    return $view;
};

// Register flash messages as "flash" in application container
$container['flash'] = function() {
    return new \Slim\Flash\Messages();
};