<?php
use STS\Middleware\GuestMiddleware;
use STS\Middleware\AuthMiddleware;
use STS\Middleware\SuperAdminMiddleware;

$app->get('/', '\STS\Controllers\HomeController:index')->setName('home');

$app->group('/ticket', function() {
    $this->get('/open[/]', '\STS\Controllers\TicketController:getOpen')->setName('ticket.open');
    $this->post('/open[/]', '\STS\Controllers\TicketController:postOpen');

    $this->get('/{emailHash}/{ticketHash}[/]', '\STS\Controllers\TicketController:view')->setName('ticket.view');
    $this->post('/{emailHash}/{ticketHash}[/]', '\STS\Controllers\TicketController:respond');
});

$app->group('/login', function() {
    $this->get('[/]', '\STS\Controllers\AuthController:getLogin')->setName('login');
    $this->post('[/]', '\STS\Controllers\AuthController:postLogin');
})->add(new GuestMiddleware($container));

$app->get('/logout[/]', '\STS\Controllers\AuthController:logout')->setName('logout');

$app->group('/admin', function() use ($container) {
    $this->get('/tickets[/]', '\STS\Controllers\AdminTicketController:list')->setName('admin.ticket.list');

    $this->group('/users', function() {
        $this->get('[/]', '\STS\Controllers\UserController:list')->setName('admin.user.list');
        $this->get('/create[/]', '\STS\Controllers\UserController:getCreate')->setName('admin.user.create');
        $this->post('/create[/]', '\STS\Controllers\UserController:postCreate');
        $this->post('/delete/{userId:[0-9]+}[/]', '\STS\Controllers\UserController:delete')->setName('admin.user.delete');
        $this->get('/{userId:[0-9]+}[/]', '\STS\Controllers\UserController:getModify')->setName('admin.user.modify');
        $this->post('/{userId:[0-9]+}[/]', '\STS\Controllers\UserController:postModify');
    })->add(new SuperAdminMiddleware($container));

    $this->group('/tickets/{ticketHash}', function() {
        $this->get('[/]', '\STS\Controllers\AdminTicketController:view')->setName('admin.ticket.view');
        $this->post('/respond[/]', '\STS\Controllers\AdminTicketController:respond')->setName('admin.ticket.respond');
        $this->post('/close[/]', '\STS\Controllers\AdminTicketController:close')->setName('admin.ticket.close');
        $this->post('/reopen[/]', '\STS\Controllers\AdminTicketController:reopen')->setName('admin.ticket.reopen');
        $this->post('/assign[/]', '\STS\Controllers\AdminTicketController:assign')->setName('admin.ticket.assign');

        $this->group('/transfer', function() {
            $this->get('[/]', '\STS\Controllers\AdminTicketController:getTransfer')->setName('admin.ticket.transfer');
            $this->post('[/]', '\STS\Controllers\AdminTicketController:postTransfer');
        });
    });
})->add(new AuthMiddleware($container));