<?php
namespace STS\Extensions;

class AuthExtension extends \Twig_Extension {
    public function __construct() {
    }

    public function getFunctions() {
        return [
            new \Twig_SimpleFunction('auth', [$this, 'auth']),
            new \Twig_SimpleFunction('user', [$this, 'user']),
        ];
    }

    public function auth() {
        return isset($_SESSION['auth']);
    }

    public function user() {
        return $_SESSION['auth'];
    }
}