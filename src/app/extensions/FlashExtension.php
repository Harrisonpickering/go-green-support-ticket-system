<?php
namespace STS\Extensions;

class FlashExtension extends \Twig_Extension {
    protected $flash;

    public function __construct(\Slim\Flash\Messages $flash) {
        $this->flash = $flash;
    }

    public function getFunctions() {
        return [
            new \Twig_SimpleFunction('flash', [$this, 'flash'])
        ];
    }

    public function flash() {
        return $this->flash->getMessages();
    }
}