<?php
namespace STS\Middleware;

class GuestMiddleware extends Middleware {
    public function __invoke($req, $res, $next) {
        if(isset($_SESSION['auth'])) {
            return $res->withRedirect($this->c->get('router')->pathFor('admin.ticket.list'));
        }

        $res = $next($req, $res);
        return $res;
    }
}