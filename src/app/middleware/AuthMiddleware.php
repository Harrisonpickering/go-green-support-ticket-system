<?php
namespace STS\Middleware;

class AuthMiddleware extends Middleware {
    public function __invoke($req, $res, $next) {
        if(!isset($_SESSION['auth'])) {
            $this->c->get('flash')->addMessage('global', 'You don\'t have access to this directory!');
            return $res->withRedirect($this->c->get('router')->pathFor('home'));
        }

        $res = $next($req, $res);
        return $res;
    }
}