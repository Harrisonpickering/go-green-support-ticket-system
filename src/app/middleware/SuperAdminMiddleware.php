<?php
namespace STS\Middleware;

class SuperAdminMiddleware extends Middleware {
    public function __invoke($req, $res, $next) {
        if($_SESSION['auth']['SuperAdmin'] == 0) {
            $this->c->get('flash')->addMessage('global', 'You don\'t have access to this directory!');
            return $res->withRedirect($this->c->get('router')->pathFor('admin.ticket.list'));
        }

        $res = $next($req, $res);
        return $res;
    }
}