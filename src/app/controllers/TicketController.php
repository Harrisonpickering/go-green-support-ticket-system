<?php
namespace STS\Controllers;

use Carbon\Carbon;

class TicketController {
    protected $view;
    protected $flash;

    protected $db;
    protected $users;
    protected $tickets;
    protected $ticketResponses;

    protected $siteHost;

    public function __construct(\Slim\Container $c) {
        $this->view = $c->get('view');
        $this->flash = $c->get('flash');

        $this->db = $c->get('db');
        $this->users = $c->get('db')->table('users');
        $this->tickets = $c->get('db')->table('tickets');
        $this->ticketResponses = $c->get('db')->table('tickets_responses');

        $this->siteHost = $c->get('settings')['site']['host'];
    }

    public function getOpen($req, $res, $args) {
        return $this->view->render($res, 'ticket.open.twig');
    }

    public function postOpen($req, $res, $args) {
        $b = $req->getParsedBody();
        if(!isset($b['email'], $b['name'], $b['subject'], $b['message'])) {
            $this->flash->addMessage('error', 'All fields are required!');
            return $res->withHeader('Location', '/ticket/open');
        }

        $ticketHash = uniqid(time());
        $emailHash = base64_encode($b['email']);
        $ticketID = $this->tickets->insertGetId([
            'TicketHash' => $ticketHash,
            'EmailHash' => $emailHash,
            'CustomerName' => $b['name'],
            'Subject' => $b['subject'],
        ]);
        $this->ticketResponses->insert([
            'TicketID' => $ticketID,
            'Text' => $b['message']
        ]);

        mail($b['email'], 'Go Green Support, "'.$b['subject'].'"', '
            Thank you for contacting Go Green Support! We will try to respond
            to your enquiry/issue as soon as we can.

            Below is the link to your ticket:
            http://'.$this->siteHost.'/ticket/'.$emailHash.'/'.$ticketHash.'/
        ', 'From: harrisonpickering@live.co.uk');

        $this->flash->addMessage('info', 'Thank you for opening a support ticket with us '.$b['name'].'! We\'ll get back you as soon as possible.');
        return $res->withHeader('Location', '/ticket/'.$emailHash.'/'.$ticketHash.'/');
    }

    public function view($req, $res, $args) {
        $ticket = $this->tickets->where('EmailHash', $args['emailHash'])->where('TicketHash', $args['ticketHash'])->first();
        $responses = $this->ticketResponses->where('TicketID', $ticket->ID)->orderBy('ID', 'DESC')->get();

        $ticket->Updated = Carbon::createFromTimestamp(strtotime($ticket->Updated))->diffForHumans();
        foreach($responses as $response) {
            $response->StaffName = $response->UserID != null ? $this->db->table('users')->find($response->UserID)->DisplayName : 'Unknown';
            $response->Created = date('d/m/Y (H:i)', strtotime($response->Created));
        }

        return $this->view->render($res, 'ticket.view.twig', [
            'ticket' => $ticket,
            'responses' => $responses,
            'emailHash' => $args['emailHash'],
        ]);
    }

    public function respond($req, $res, $args) {
        $emailHash = $args['emailHash'];
        $ticketHash = $args['ticketHash'];

        $b = $req->getParsedBody();
        if(!isset($b['message'])) {
            $this->flash->addMessage('error', 'All fields are required!');
            return $res->withHeader('Location', '/ticket/'.$emailHash.'/'.$ticketHash.'/');
        }

        $ticket = $this->tickets->where('EmailHash', $emailHash)->where('TicketHash', $ticketHash)->first();
        $this->ticketResponses->insert([
            'TicketID' => $ticket->ID,
            'Text' => $b['message'],
        ]);
        $this->tickets->where('ID', $ticket->ID)->update([
            'Responded' => 0,
            'Updated' => date('Y-m-d H:i:s', time()),
        ]);

        $this->flash->addMessage('info', 'Thanks for replying to the ticket! We\'ll get back to you as soon as possible.');
        return $res->withHeader('Location', '/ticket/'.$emailHash.'/'.$ticketHash.'/');
    }
}