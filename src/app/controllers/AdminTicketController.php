<?php
namespace STS\Controllers;

use Carbon\Carbon;

class AdminTicketController {
    protected $view;
    protected $flash;

    protected $db;
    protected $users;
    protected $tickets;
    protected $ticketResponses;

    protected $siteHost;

    public function __construct(\Slim\Container $c) {
        $this->view = $c->get('view');
        $this->flash = $c->get('flash');

        $this->db = $c->get('db');
        $this->users = $c->get('db')->table('users');
        $this->tickets = $c->get('db')->table('tickets');
        $this->ticketResponses = $c->get('db')->table('tickets_responses');

        $this->siteHost = $c->get('settings')['site']['host'];
    }

    private function queryFilter($qb, $query) {
        // Assigned filter
        if(isset($query['a']) && $query['a'] != 0) {
            if($query['a'] == 1) {
                $qb->where('UserID', null);
            } else {
                $qb->where('UserID', $_SESSION['auth']['ID']);
            }
        } else {
            $qb->where(function($query) {
                $query->where('UserID', null)->orWhere('UserID', $_SESSION['auth']['ID']);
            });
        }

        // Client filter
        $wheres = [];
        if(isset($query['c']) && trim($query['c']) != '') {
            $wheres[] = ['EmailHash', '=', $query['c']];
        }

        // Ticket filter
        if(isset($query['t']) && trim($query['t']) != '') {
            $wheres[] = ['Subject', 'like', '%'.$query['t'].'%'];
        }

        // Status filter
        if(isset($query['s']) && $query['s'] != 0) {
            $wheres[] = ['Responded', '=', $query['s'] - 1];
        }

        // Open/close filter
        if(isset($query['oc']) && $query['oc'] != 0) {
            $wheres[] = ['Closed', '=', $query['oc'] - 1];
        }

        return $qb->where($wheres);
    }

    public function list($req, $res, $args) {
        $query = $req->getQueryParams();
        $query['a'] = isset($query['a']) ? (int)$query['a'] : null;
        $query['s'] = isset($query['s']) ? (int)$query['s'] : null;
        $query['oc'] = isset($query['oc']) ? (int)$query['oc'] : null;

        $page = $req->getQueryParam('page') ? $req->getQueryParam('page') : 1;
        $limit = $req->getQueryParam('l') ? $req->getQueryParam('l') : 10;

        $total = $this->queryFilter($this->db->table('tickets'), $query)->count();

        $pages = ceil($total / $limit);
        $start = ($page - 1) * $limit;

        $tickets = $this->queryFilter($this->db->table('tickets'), $query)->limit($limit)->offset($start)->get();

        $clients = $this->db->table('tickets')->get(['EmailHash', 'CustomerName']);
        foreach($clients as $client) {
            $client->Email = base64_decode($client->EmailHash);
        }

        foreach($tickets as $ticket) {
            $ticket->Email = base64_decode($ticket->EmailHash);
            $ticket->Assigned = $ticket->UserID != null ? $this->users->find($ticket->UserID)->DisplayName : 'None';
            $ticket->Updated = Carbon::createFromTimestamp(strtotime($ticket->Updated))->diffForHumans();
        }

        unset($query['page']);
        $queryString = http_build_query($query);
        return $this->view->render($res, 'admin/ticket.list.twig', [
            'query' => $query,
            'queryString' => $queryString,
            'clients' => $clients,
            'tickets' => $tickets,
            'pagination' => [
                'page' => $page,
                'pages' => $pages,
                'total' => $total,
                'start' => $start,
                'end' => min($start + $limit, $total),
            ],
        ]);
    }

    public function view($req, $res, $args) {
        $ticket = $this->tickets->where('TicketHash', $args['ticketHash'])->first();
        if($ticket == null) {
            $this->flash->addMessage('global', 'This ticket doesn\'t exist!');
            return $res->withHeader('Location', '/admin/tickets/');
        }
        if($ticket->UserID != null && $ticket->UserID != $_SESSION['auth']['ID']) {
            $this->flash->addMessage('global', 'This ticket isn\'t assigned to you!');
            return $res->withHeader('Location', '/admin/tickets/');
        }

        $ticket->Updated = Carbon::createFromTimestamp(strtotime($ticket->Updated))->diffForHumans();

        $responses = $this->ticketResponses->where('TicketID', $ticket->ID)->orderBy('ID', 'DESC')->get();
        foreach($responses as $response) {
            $response->StaffName = $response->UserID != null ? $this->db->table('users')->find($response->UserID)->DisplayName : 'Unknown';
            $response->Created = date('d/m/Y (H:i)', strtotime($response->Created));
        }

        return $this->view->render($res, 'admin/ticket.view.twig', [
            'ticket' => $ticket,
            'responses' => $responses,
        ]);
    }

    public function respond($req, $res, $args) {
        $ticketHash = $args['ticketHash'];
        $ticket = $this->tickets->where('TicketHash', $ticketHash)->first();
        if($ticket == null) {
            $this->flash->addMessage('global', 'This ticket doesn\'t exist!');
            return $res->withHeader('Location', '/admin/tickets/');
        }
        if($ticket->UserID != null && $ticket->UserID != $_SESSION['auth']['ID']) {
            $this->flash->addMessage('global', 'This ticket isn\'t assigned to you!');
            return $res->withHeader('Location', '/admin/tickets/');
        }

        $b = $req->getParsedBody();
        if(!isset($b['message'])) {
            $this->flash->addMessage('error', 'All fields are required!');
            return $res->withHeader('Location', '/admin/tickets/'.$ticketHash.'/');
        }

        $this->ticketResponses->insert([
            'TicketID' => $ticket->ID,
            'UserID' => $_SESSION['auth']['ID'],
            'Text' => $b['message'],
        ]);
        $this->tickets->where('ID', $ticket->ID)->update([
            'Responded' => 1,
            'Updated' => date('Y-m-d H:i:s', time()),
        ]);

        mail(base64_decode($ticket->EmailHash), 'Go Green Support, "'.$ticket->Subject.'"', '
            Your ticket has been replied to!

            Below is the link to your ticket:
            http://'.$this->siteHost.'/ticket/'.$ticket->EmailHash.'/'.$ticket->TicketHash.'/
        ', 'From: harrisonpickering@live.co.uk');

        $this->flash->addMessage('info', 'Replied to ticket!');
        return $res->withHeader('Location', '/admin/tickets/'.$ticketHash.'/');
    }

    public function close($req, $res, $args) {
        $ticketHash = $args['ticketHash'];
        $ticket = $this->tickets->where('TicketHash', $ticketHash)->first();
        if($ticket == null) {
            $this->flash->addMessage('global', 'This ticket doesn\'t exist!');
            return $res->withHeader('Location', '/admin/tickets/');
        }
        if($ticket->UserID != null && $ticket->UserID != $_SESSION['auth']['ID']) {
            $this->flash->addMessage('global', 'This ticket isn\'t assigned to you!');
            return $res->withHeader('Location', '/admin/tickets/');
        }

        $this->tickets->where('ID', $ticket->ID)->update([
            'Closed' => 1
        ]);

        mail(base64_decode($ticket->EmailHash), 'Go Green Support, "'.$ticket->Subject.'"', '
            Your ticket has been closed.

            Below is the link to your ticket:
            http://'.$this->siteHost.'/ticket/'.$ticket->EmailHash.'/'.$ticket->TicketHash.'/
        ', 'From: harrisonpickering@live.co.uk');

        $this->flash->addMessage('info', 'Ticket has been closed!');
        return $res->withHeader('Location', '/admin/tickets/'.$ticketHash.'/');
    }

    public function reopen($req, $res, $args) {
        $ticketHash = $args['ticketHash'];
        $ticket = $this->tickets->where('TicketHash', $ticketHash)->first();
        if($ticket == null) {
            $this->flash->addMessage('global', 'This ticket doesn\'t exist!');
            return $res->withHeader('Location', '/admin/tickets/');
        }
        if($ticket->UserID != null && $ticket->UserID != $_SESSION['auth']['ID']) {
            $this->flash->addMessage('global', 'This ticket isn\'t assigned to you!');
            return $res->withHeader('Location', '/admin/tickets/');
        }

        $this->tickets->where('ID', $ticket->ID)->update([
            'Closed' => 0
        ]);

        mail(base64_decode($ticket->EmailHash), 'Go Green Support, "'.$ticket->Subject.'"', '
            Your ticket has been reopened.

            Below is the link to your ticket:
            http://'.$this->siteHost.'/ticket/'.$ticket->EmailHash.'/'.$ticket->TicketHash.'/
        ', 'From: harrisonpickering@live.co.uk');

        $this->flash->addMessage('info', 'Ticket has been reopened.');
        return $res->withHeader('Location', '/admin/tickets/'.$ticketHash.'/');
    }

    public function assign($req, $res, $args) {
        $ticketHash = $args['ticketHash'];
        $ticket = $this->tickets->where('TicketHash', $ticketHash)->first();
        if($ticket == null) {
            $this->flash->addMessage('global', 'This ticket doesn\'t exist!');
            return $res->withHeader('Location', '/admin/tickets/');
        }
        if($ticket->UserID != null && $ticket->UserID != $_SESSION['auth']['ID']) {
            $this->flash->addMessage('global', 'This ticket isn\'t assigned to you!');
            return $res->withHeader('Location', '/admin/tickets/');
        }

        $this->tickets->where('ID', $ticket->ID)->update([
            'UserID' => $_SESSION['auth']['ID'],
        ]);

        $this->flash->addMessage('info', 'You\'ve assigned the ticket to yourself.');
        return $res->withHeader('Location', '/admin/tickets/'.$ticketHash.'/');
    }

    public function getTransfer($req, $res, $args) {
        $ticketHash = $args['ticketHash'];
        $ticket = $this->tickets->where('TicketHash', $ticketHash)->first();
        if($ticket == null) {
            $this->flash->addMessage('global', 'This ticket doesn\'t exist!');
            return $res->withHeader('Location', '/admin/tickets/');
        }
        if($ticket->UserID != null && $ticket->UserID != $_SESSION['auth']['ID']) {
            $this->flash->addMessage('global', 'This ticket isn\'t assigned to you!');
            return $res->withHeader('Location', '/admin/tickets/');
        }

        $users = $this->users->get(['ID', 'DisplayName']);

        return $this->view->render($res, 'admin/ticket.transfer.twig', [
            'ticket' => $ticket,
            'users' => $users,
        ]);
    }

    public function postTransfer($req, $res, $args) {
        $ticketHash = $args['ticketHash'];
        $ticket = $this->tickets->where('TicketHash', $ticketHash)->first();
        if($ticket == null) {
            $this->flash->addMessage('global', 'This ticket doesn\'t exist!');
            return $res->withHeader('Location', '/admin/tickets/');
        }
        if($ticket->UserID != null && $ticket->UserID != $_SESSION['auth']['ID']) {
            $this->flash->addMessage('global', 'This ticket isn\'t assigned to you!');
            return $res->withHeader('Location', '/admin/tickets/');
        }

        $b = $req->getParsedBody();
        if(!isset($b['user'])) {
            $this->flash->addMessage('error', 'All fields are required!');
            return $res->withHeader('Location', '/admin/tickets/'.$ticketHash.'/transfer/');
        }

        $userSegments = explode('.', $b['user']);
        $this->tickets->where('ID', $ticket->ID)->update([
            'UserID' => $userSegments[0]
        ]);

        $this->flash->addMessage('global', 'Transferred ticket to '.$userSegments[1].'!');
        return $res->withHeader('Location', '/admin/tickets/');
    }
}