<?php
namespace STS\Controllers;

class HomeController {
    protected $view;

    public function __construct(\Slim\Container $c) {
        $this->view = $c->get('view');
    }

    public function index($req, $res, $args) {
        return $this->view->render($res, 'home.twig');
    }
}