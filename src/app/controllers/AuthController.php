<?php
namespace STS\Controllers;

class AuthController {
    protected $view;
    protected $flash;

    protected $users;

    public function __construct(\Slim\Container $c) {
        $this->view = $c->get('view');
        $this->flash = $c->get('flash');

        $this->users = $c->get('db')->table('users');
    }

    public function getLogin($req, $res, $args) {
        return $this->view->render($res, 'login.twig');
    }

    public function postLogin($req, $res, $args) {
        $b = $req->getParsedBody();
        if(!isset($b['username'], $b['password'])) {
            $this->flash->addMessage('error', 'All fields are required!');
            return $res->withHeader('Location', '/login/');
        }

        $user = $this->users->where('Username', strtolower($b['username']))->where('SoftDelete', 0)->first();
        if($user == null) {
            $this->flash->addMessage('error', 'User "'.$b['username'].'" doesn\'t exist!');
            return $res->withHeader('Location', '/login/');
        }

        if(!password_verify($b['password'], $user->Password)) {
            $this->flash->addMessage('error', 'Password is incorrect!');
            return $res->withHeader('Location', '/login/');
        }

        $_SESSION['auth'] = [
            'ID' => $user->ID,
            'SuperAdmin' => $user->SuperAdmin,
            'DisplayName' => $user->DisplayName,
            'Username' => $user->Username,
        ];

        $this->flash->addMessage('global', 'Welcome back '.$user->DisplayName.'!');
        return $res->withHeader('Location', '/admin/tickets/');
    }

    public function logout($req, $res, $args) {
        unset($_SESSION['auth']);
        $this->flash->addMessage('global', 'Logged out!');
        return $res->withHeader('Location', '/');
    }
}