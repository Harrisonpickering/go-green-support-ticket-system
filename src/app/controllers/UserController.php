<?php
namespace STS\Controllers;

use Carbon\Carbon;

class UserController {
    protected $view;
    protected $flash;

    protected $db;
    protected $users;

    public function __construct(\Slim\Container $c) {
        $this->view = $c->get('view');
        $this->flash = $c->get('flash');

        $this->db = $c->get('db');
        $this->users = $c->get('db')->table('users');
    }

    public function list($req, $res, $args) {
        $page = $req->getQueryParam('page') ? $req->getQueryParam('page') : 1;
        $total = $this->users->where('SoftDelete', 0)->count();
        $limit = 10;
        $pages = ceil($total / $limit);
        $start = ($page - 1) * $limit;
        $users = $this->users->where('SoftDelete', 0)->limit($limit)->offset($start)->get();

        foreach($users as $user) {
            $user->Created = date('d/m/Y (H:i)', strtotime($user->Created));
        }

        return $this->view->render($res, 'admin/user.list.twig', [
            'users' => $users,
            'pagination' => [
                'page' => $page,
                'pages' => $pages,
                'total' => $total,
                'start' => $start,
                'end' => min($start + $limit, $total),
            ],
        ]);
    }

    public function getCreate($req, $res, $args) {
        return $this->view->render($res, 'admin/user.create.twig');
    }

    public function postCreate($req, $res, $args) {
        $b = $req->getParsedBody();
        if(!isset($b['displayName'], $b['username'], $b['adminPassword'])) {
            $this->flash->addMessage('error', 'All fields are required!');
            return $res->withHeader('Location', '/admin/users/create/');
        }

        $adminUser = $this->db->table('users')->where('ID', $_SESSION['auth']['ID'])->first(['Password']);
        if($adminUser == null) {
            $this->flash->addMessage('global', 'You\'re not authorised to do this!');
            return $res->withHeader('Location', '/admin/users/');
        }
        if(!password_verify($b['adminPassword'], $adminUser->Password)) {
            $this->flash->addMessage('error', 'Admin password is incorrect!');
            return $res->withHeader('Location', '/admin/users/create/');
        }

        $user = $this->users->where('Username', strtolower($b['username']))->first();
        if($user != null) {
            $this->users->where('ID', $user->ID)->update([
                'DisplayName' => $b['displayName'],
                'Password' => password_hash($b['password'], PASSWORD_DEFAULT),
                'SuperAdmin' => $b['superAdmin'] == 'on',
                'SoftDelete' => 0,
            ]);
            $this->flash->addMessage('global', 'Created user '.$b['username'].'!');
            return $res->withHeader('Location', '/admin/users/');
        }

        $this->users->insert([
            'DisplayName' => $b['displayName'],
            'Username' => strtolower($b['username']),
            'Password' => password_hash($b['password'], PASSWORD_DEFAULT),
            'SuperAdmin' => $b['superAdmin'] == 'on'
        ]);

        $this->flash->addMessage('global', 'Created user '.$b['username'].'!');
        return $res->withHeader('Location', '/admin/users/');
    }

    public function getModify($req, $res, $args) {
        $user = $this->users->find($args['userId']);
        if($user == null) {
            $this->flash->addMessage('global', 'This user doesn\'t exist!');
            return $res->withHeader('Location', '/admin/users/');
        }

        return $this->view->render($res, 'admin/user.view.twig', [
            'user' => $user,
        ]);
    }

    public function postModify($req, $res, $args) {
        $user = $this->users->find($args['userId']);
        if($user == null) {
            $this->flash->addMessage('global', 'This user doesn\'t exist!');
            return $res->withHeader('Location', '/admin/users/');
        }

        $b = $req->getParsedBody();
        if(!isset($b['displayName'], $b['username'], $b['adminPassword'])) {
            $this->flash->addMessage('error', 'All fields are required!');
            return $res->withHeader('Location', '/admin/users/'.$user->ID.'/');
        }

        $adminUser = $this->db->table('users')->where('ID', $_SESSION['auth']['ID'])->first(['Password']);
        if($adminUser == null) {
            $this->flash->addMessage('global', 'You\'re not authorised to do this!');
            return $res->withHeader('Location', '/admin/users/');
        }
        if(!password_verify($b['adminPassword'], $adminUser->Password)) {
            $this->flash->addMessage('error', 'Admin password is incorrect!');
            return $res->withHeader('Location', '/admin/users/'.$user->ID.'/');
        }

        $this->users->where('ID', $user->ID)->update([
            'DisplayName' => $b['displayName'],
            'Username' => strtolower($b['username']),
            'Password' => $b['password'] != '' ? password_hash($b['password'], PASSWORD_DEFAULT) : $user->Password,
            'SuperAdmin' => $b['superAdmin'] == 'on'
        ]);

        $this->flash->addMessage('global', 'Updated user!');
        return $res->withHeader('Location', '/admin/users/');
    }

    public function delete($req, $res, $args) {
        $userId = $args['userId'];

        $b = $req->getParsedBody();
        if(!isset($b['adminPassword'])) {
            $this->flash->addMessage('error', 'Admin password is required to delete user!');
            return $res->withHeader('Location', '/admin/users/'.$user->ID.'/');
        }

        $adminUser = $this->db->table('users')->where('ID', $_SESSION['auth']['ID'])->first(['Password']);
        if($adminUser == null) {
            $this->flash->addMessage('global', 'You\'re not authorised to do this!');
            return $res->withHeader('Location', '/admin/users/');
        }
        if(!password_verify($b['adminPassword'], $adminUser->Password)) {
            $this->flash->addMessage('error', 'Admin password is incorrect!');
            return $res->withHeader('Location', '/admin/users/'.$user->ID.'/');
        }

        $this->users->where('ID', $userId)->update([
            'SoftDelete' => 1
        ]);

        $this->flash->addMessage('global', 'User has been deleted!');
        return $res->withHeader('Location', '/admin/users/');
    }
}