<?php
// Load configuration from dotenv (.env)
$dotenv = new Dotenv\Dotenv(__DIR__.'/../');
$dotenv->load();

// Return configuration
return [
    'settings' => [
        'displayErrorDetails' => getenv('SLIM_DISPLAY_ERROR_DETAILS'),
        'logger' => [
            'name' => getenv('SLIM_LOGGER_NAME'),
            'level' => Monolog\Logger::DEBUG,
            'path' => __DIR__.'/../logs/'.getenv('SLIM_LOGGER_NAME').'.log',
        ],
        'site' => [
            'host' => getenv('SITE_HOST'),
        ],
        'db' => [
            'driver' => getenv('DB_DRIVER'),
            'host' => getenv('DB_HOST'),
            'database' => getenv('DB_NAME'),
            'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASS'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
    ],
];